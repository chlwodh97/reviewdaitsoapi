package com.daitso.reviewdaitsoapi.repository;

import com.daitso.reviewdaitsoapi.entity.Writing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface WritingRepository extends JpaRepository<Writing, Long> {


    /**
     * 게시글 제목의 검색 기능
     */
    List<Writing> findByTitleContaining(String keyword);

    /**
     * 최신순 정렬 > 아이디를 오름차순으로 변경
     */
    List<Writing> findAllByOrderByIdDesc();

    /**
     * (페이징)최신순 정렬 > 아이디를 오름차순으로 변경
     */
    Page<Writing> findAllByOrderByIdDesc(Pageable pageable);


    /**
     * 게시글 내용의 검색기능
     */
    List<Writing> findByContentContaining(String keyword);

    /**
     * 해당 ID가 작성한 게시글 전부 모아보기
     */
    List<Writing> findByMember_Id(long id);

    Page<Writing> findByMember_Id(Pageable pageable,long id);

    /**
     * 조회수 별 정렬
     */
    List<Writing> findAllByOrderByViewNumberDesc();

    /**
     * 추천수 별 정렬
     */
    List<Writing> findAllByOrderByHitNumberDesc();

    /**
     * (페이징)추천수 별 정렬
     */
    Page<Writing> findAllByOrderByHitNumberDesc(Pageable pageable);

    /**
     * 차트를 위한 날짜 찾기
     */
    Long countByDateCreate(LocalDateTime dateCreate);

    /**
     * 굿즈 id로 게시글 찾기
     */
//    List<Writing> findByGoods_Id(long id);
    
    List<Writing> findByGoods_Id(long id);

    Page<Writing> findByGoods_Id(Pageable pageable, long id );

    Page<Writing> findByCategory_Id(Pageable pageable, long id );

    /**
     * 차트를 위한 날짜 찾기 (LocalDateTime)
     */
    Long countByDateCreateBetween(LocalDateTime startDate , LocalDateTime endDate);

    List<Writing> findByCategory_Id(long id);

}

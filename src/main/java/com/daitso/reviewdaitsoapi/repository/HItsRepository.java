package com.daitso.reviewdaitsoapi.repository;

import com.daitso.reviewdaitsoapi.entity.Hits;
import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HItsRepository extends JpaRepository<Hits , Long> {

    Optional<Hits> findByMemberAndWriting(Member member, Writing writing);

    Optional<Hits> deleteByMemberAndWriting(Member member , Writing writing);


    List<Hits> findByWritingId(long id);



}

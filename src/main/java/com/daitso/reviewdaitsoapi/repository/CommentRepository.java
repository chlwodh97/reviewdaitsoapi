package com.daitso.reviewdaitsoapi.repository;

import com.daitso.reviewdaitsoapi.entity.Comment;
import com.daitso.reviewdaitsoapi.entity.Hits;
import com.daitso.reviewdaitsoapi.entity.Writing;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {



    List<Comment> findByMember_Id(long id);

    List<Comment> findAllByOrderByIdDesc();

    List<Comment> findByWritingId(long id);

}

package com.daitso.reviewdaitsoapi.repository;

import com.daitso.reviewdaitsoapi.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}

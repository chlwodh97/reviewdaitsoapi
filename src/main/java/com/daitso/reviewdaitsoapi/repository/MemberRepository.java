package com.daitso.reviewdaitsoapi.repository;

import com.daitso.reviewdaitsoapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {

    Long countByDateJoin(LocalDateTime datejoin);

    Long countByDateJoinBetween(LocalDateTime startDate , LocalDateTime endDate);

    Optional<Member> findByHomeIdAndPassword(String homeid , String password);


    // 서비스에서 이름 일치하는애 찾기 위해
    Optional<Member> findByUsername(String username);
}

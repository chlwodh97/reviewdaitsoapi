package com.daitso.reviewdaitsoapi.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Goods {

    @Id
    private Long id;

    @JoinColumn(name = "categoryId" , nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

    @Column(nullable = false)
    private String goodsName;

    @Column(nullable = false)
    private Double goodsPrice;
}

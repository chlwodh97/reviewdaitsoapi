package com.daitso.reviewdaitsoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Writing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "memberId" , nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Member member;

    @JoinColumn(name = "categoryId" , nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

    @JoinColumn(name = "goodsId" , nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Goods goods;

    @Column(nullable = false , length = 50)
    private String title;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private Integer viewNumber;

    @Column(nullable = false)
    private Integer hitNumber;

    @Column(nullable = false)
    private Integer commentNumber;

    private String imgUrl;
}

package com.daitso.reviewdaitsoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Hits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "memberId" , nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Member member;

    @JoinColumn(name = "writingId" , nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Writing writing;

}

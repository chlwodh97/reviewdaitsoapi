package com.daitso.reviewdaitsoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    private String imgUrl;

    @Column(nullable = false, length = 10)
    private String categoryType;

    @Column(nullable = false,  length = 10)
    private String bigCategory;

    @Column(nullable = false, length = 200)
    private String etcMemo;
}

package com.daitso.reviewdaitsoapi.entity;

import com.daitso.reviewdaitsoapi.enums.MemberLevel;
import com.daitso.reviewdaitsoapi.enums.MemberStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

@Entity
@Getter
@Setter
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,length = 20, unique = true)
    private String username;

    @Column(nullable = false, length = 20)
    private String password;

    @Column(nullable = false, length = 16, unique = true)
    private String nickName;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MemberStatus status;

    @Column(nullable = false)
    private LocalDateTime dateJoin;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING) // Enum으로 권한 설정
    private MemberLevel level;

    @Column(nullable = false)
    private Boolean isAdmin;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority(level.toString()));
        // 컬렉션 -> 수집하는 방법  싱글톤 -> 권한은 하나라고 확실히 얘기를 해준다 |
        // SimpleGrantedAuthority의 필요타입은 String이기 때문에 toString 해줌
    }


    @Override // 만료 안됐니? -> 언제 까지 쓸 수 있나 유효기간 정하기
              // 넷플같은 유료 회원 돈 안냈을 때 정도
    public boolean isAccountNonExpired() {
        return true; //만료 개념이 없으면 트루
    }

    @Override // 정지 안됐니? -> 정지여부 판단
    public boolean isAccountNonLocked() {
        return true; //가둘곳 개념 없으면 트루
    }

    @Override // 너에 대한 인증 만료 됐니? -> 개인 신분 확인
              // 개인에 대한 신변 확인
    public boolean isCredentialsNonExpired() {
        return true; // 기능 없으면 트루
    }

    @Override // 활성화 됐니 -> 휴면 회원 활성화가 되지 않았음
    public boolean isEnabled() {
        return true; // 활성화 개념 없으면 트루
    }
}

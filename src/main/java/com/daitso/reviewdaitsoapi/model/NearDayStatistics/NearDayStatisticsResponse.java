package com.daitso.reviewdaitsoapi.model.NearDayStatistics;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class NearDayStatisticsResponse {
    private List<String> labels;

    private List<Long> datasets;
}

package com.daitso.reviewdaitsoapi.model.category;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryRequest {
    private String name;
    private String imgUrl;

    private String categoryType;
    private String bigCategory;
    private String etcMemo;
}

package com.daitso.reviewdaitsoapi.model.category;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryNameChangeRequest {
    private String name;
}

package com.daitso.reviewdaitsoapi.model.category;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryResponse {
    private Long id;
    private String name;
    private String imgUrl;
    private String categoryType;
    private String bigCategory;
    private String etcMemo;

}

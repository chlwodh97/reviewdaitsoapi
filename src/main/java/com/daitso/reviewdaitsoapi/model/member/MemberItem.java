package com.daitso.reviewdaitsoapi.model.member;

import com.daitso.reviewdaitsoapi.enums.MemberLevel;
import com.daitso.reviewdaitsoapi.enums.MemberStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MemberItem {
    private Long id;
    private String homeId;
    private String nickName;
    private LocalDateTime dateJoin;
    private MemberStatus status;
    private MemberLevel level;
    private String isAdmin;
}

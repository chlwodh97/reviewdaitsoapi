package com.daitso.reviewdaitsoapi.model.member;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
    private String homeId;
    private String Password;
}

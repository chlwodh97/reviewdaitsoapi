package com.daitso.reviewdaitsoapi.model.member;

import com.daitso.reviewdaitsoapi.enums.MemberLevel;
import com.daitso.reviewdaitsoapi.enums.MemberStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String homeId;
    private String password;
    private String nickName;
    private MemberStatus status;
    private LocalDateTime dateJoin;
    private MemberLevel level;
    private Boolean isAdmin;
}

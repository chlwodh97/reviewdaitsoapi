package com.daitso.reviewdaitsoapi.model.member;

import com.daitso.reviewdaitsoapi.enums.MemberStatus;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberChangeStatusRequest {
    private MemberStatus status;
}

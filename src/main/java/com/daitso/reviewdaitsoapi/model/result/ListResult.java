package com.daitso.reviewdaitsoapi.model.result;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list;
    private Long totalCount; // 총 데이터개수
    private Integer totalPage; // 총 페이지수 -> 1 페
    private Integer currentPage; // 현재 페이지번호 -1
}

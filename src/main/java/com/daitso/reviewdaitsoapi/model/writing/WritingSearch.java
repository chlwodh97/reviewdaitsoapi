package com.daitso.reviewdaitsoapi.model.writing;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WritingSearch {
    private Long id;
    private String title;
    private String content;
    private Integer viewNumber;
    private Integer hitNumber;
    private Integer commentNumber;

    private Long memberId;
    private String nickName;
    private String isAdmin;

    private Long categoryId;
    private String categoryName;

    private Long goodsId;

}

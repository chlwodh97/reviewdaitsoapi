package com.daitso.reviewdaitsoapi.model.writing;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class WritingItem {

    private Long id;
    private String title;
    private String content;
    private Integer viewNumber;
    private Integer hitNumber;
    private Integer commentNumber;
    private LocalDateTime dateCreate;
    private String imgUrl;


    private Long memberId;
    private String nickName;
    private String isAdmin;

    private Long categoryId;
    private String categoryName;

    private Long goodsId;
}

package com.daitso.reviewdaitsoapi.model.writing;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class WritingChangeRequest {
    private String title;
    private String content;

}

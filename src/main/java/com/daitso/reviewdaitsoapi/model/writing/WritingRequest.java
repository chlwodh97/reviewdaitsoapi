package com.daitso.reviewdaitsoapi.model.writing;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class WritingRequest {
    private String title;
    private String content;
    private String imgUrl;

}

package com.daitso.reviewdaitsoapi.model.writing;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class WritingSearchId {
    private Long id;
    private String title;
    private String content;
    private LocalDateTime dateCreate;

    private Long memberId;
    private String nickName;

    private Long categoryId;
    private String categoryName;

    private Long goodsId;
}

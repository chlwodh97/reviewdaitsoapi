package com.daitso.reviewdaitsoapi.model.goods;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodsRequest {
    private Long id;
    private String goodsName;
    private Double goodsPrice;
}

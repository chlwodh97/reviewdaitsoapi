package com.daitso.reviewdaitsoapi.model.goods;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodsPriceChangeRequest {
    private Double goodsPrice;
}

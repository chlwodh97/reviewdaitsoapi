package com.daitso.reviewdaitsoapi.model.goods;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoodsNameChangeRequest {
    private String goodsName;
}

package com.daitso.reviewdaitsoapi.model.hits;


import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HitsRequest {
    private Member member;
    private Writing writing;
}

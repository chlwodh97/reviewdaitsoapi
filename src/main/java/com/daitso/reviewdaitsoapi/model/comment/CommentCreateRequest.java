package com.daitso.reviewdaitsoapi.model.comment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentCreateRequest {
    private String content;
}

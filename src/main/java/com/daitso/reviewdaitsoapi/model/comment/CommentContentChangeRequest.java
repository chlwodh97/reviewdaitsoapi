package com.daitso.reviewdaitsoapi.model.comment;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentContentChangeRequest {
    private String content;
}

package com.daitso.reviewdaitsoapi.model.comment;

import com.daitso.reviewdaitsoapi.entity.Writing;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentSearchId {
    private String content;
    private LocalDateTime dateComment;

    private Long memberId;
    private String nickName;

    private Long categoryId;
    private String categoryName;

    private Long writingId;
    private String writingTitle;
}

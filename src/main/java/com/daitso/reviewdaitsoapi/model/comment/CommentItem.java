package com.daitso.reviewdaitsoapi.model.comment;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentItem {
    private Long id;
    private String content;
    private LocalDateTime dateComment;

    private Long memberId;
    private String nickName;
    private String isAdmin;

    private Long categoryId;

    private Long writingId;
    private String writingTitle;
}

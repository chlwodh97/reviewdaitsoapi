package com.daitso.reviewdaitsoapi.service;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Comment;
import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.enums.ResultCode;
import com.daitso.reviewdaitsoapi.model.comment.CommentContentChangeRequest;
import com.daitso.reviewdaitsoapi.model.comment.CommentCreateRequest;
import com.daitso.reviewdaitsoapi.model.comment.CommentItem;
import com.daitso.reviewdaitsoapi.model.comment.CommentSearchId;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.writing.WritingItem;
import com.daitso.reviewdaitsoapi.model.writing.WritingSearchId;
import com.daitso.reviewdaitsoapi.repository.CommentRepository;
import com.daitso.reviewdaitsoapi.repository.WritingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final WritingRepository writingRepository;

    public void setComment(CommentCreateRequest request, Category category, Member member, Writing writing) {

        Writing originData = writingRepository.findById(writing.getId()).orElseThrow();
        originData.setCommentNumber(originData.getCommentNumber()+1);

        writingRepository.save(originData);


        Comment addData = new Comment();
        addData.setCategory(category);
        addData.setMember(member);
        addData.setWriting(writing);
        addData.setContent(request.getContent());
        addData.setDateComment(LocalDateTime.now());


        commentRepository.save(addData);

    }

    public List<CommentItem> getComments() {
        List<Comment> originData = commentRepository.findAll();

        List<CommentItem> result = new LinkedList<>();
        for(Comment comment : originData){
            CommentItem addItem = new CommentItem();

            addItem.setMemberId(comment.getMember().getId());
            addItem.setNickName(comment.getMember().getNickName());
            addItem.setIsAdmin(comment.getMember().getIsAdmin()? "관리자" : "회원");

            addItem.setCategoryId(comment.getCategory().getId());

            addItem.setWritingId(comment.getWriting().getId());
            addItem.setWritingTitle(comment.getWriting().getTitle());

            addItem.setId(comment.getId());
            addItem.setContent(comment.getContent());
            addItem.setDateComment(LocalDateTime.now());

            result.add(addItem);
        }
        return result;
    }



    public void putCommentContent(Long id, CommentContentChangeRequest request) {
        Comment originData = commentRepository.findById(id).orElseThrow();
        originData.setContent(request.getContent());

        commentRepository.save(originData);
    }

    /**
     * 댓글 삭제 .. 하면 자동으로 게시글 댓글수 -1
     */

    public void delComment(Long id) {
        commentRepository.deleteById(id);

        Optional<Comment> comment = commentRepository.findById(id);
        comment.get().getWriting().setCommentNumber(comment.get().getWriting().getCommentNumber()-1);


        writingRepository.save(comment.get().getWriting());


    }


    /**
     * id 해당하는 댓글
     */
    public List<CommentSearchId> searchCommentId(long id) {
        List<Comment> originList = commentRepository.findByMember_Id(id);

        List<CommentSearchId> result = new LinkedList<>();

        for (Comment comment : originList) {

            CommentSearchId addItem = new CommentSearchId();
            addItem.setMemberId(comment.getMember().getId());
            addItem.setNickName(comment.getMember().getNickName());

            addItem.setCategoryId(comment.getCategory().getId());
            addItem.setCategoryName(comment.getCategory().getName());

            addItem.setDateComment(LocalDateTime.now());
            addItem.setContent(comment.getContent());

            addItem.setWritingId(comment.getWriting().getId());
            addItem.setWritingTitle(comment.getWriting().getTitle());


            result.add(addItem);
        }
        return result;
    }

    /**
     * 댓글 최신순으로 정렬하깅
     */
    public List<CommentSearchId> getListOrder(){

        List<Comment> originList = commentRepository.findAllByOrderByIdDesc();

        List<CommentSearchId> result = new LinkedList<>();

        for (Comment comment : originList){
            CommentSearchId addItem = new CommentSearchId();
            addItem.setMemberId(comment.getMember().getId());
            addItem.setNickName(comment.getMember().getNickName());

            addItem.setCategoryId(comment.getCategory().getId());
            addItem.setCategoryName(comment.getCategory().getName());

            addItem.setDateComment(LocalDateTime.now());
            addItem.setContent(comment.getContent());

            addItem.setWritingId(comment.getWriting().getId());
            addItem.setWritingTitle(comment.getWriting().getTitle());

            result.add(addItem);
        }
        return result;
    }

    public ListResult<CommentItem> getCommentsPage(int pageNum) {

        PageRequest pageRequest = PageRequest.of(pageNum -1,10 , Sort.by(Sort.Direction.ASC,"id"));

        Page<Comment> comments = commentRepository.findAll(pageRequest);

        List<CommentItem> result = new LinkedList<>();
        for(Comment comment : comments.getContent()){
            CommentItem addItem = new CommentItem();

            addItem.setMemberId(comment.getMember().getId());
            addItem.setNickName(comment.getMember().getNickName());
            addItem.setIsAdmin(comment.getMember().getIsAdmin()? "관리자" : "회원");

            addItem.setCategoryId(comment.getCategory().getId());

            addItem.setWritingId(comment.getWriting().getId());
            addItem.setWritingTitle(comment.getWriting().getTitle());

            addItem.setId(comment.getId());
            addItem.setContent(comment.getContent());
            addItem.setDateComment(LocalDateTime.now());

            result.add(addItem);
        }
        ListResult<CommentItem> result1 = new ListResult<>();
        //getContent 라는 애가 그냥 List의 내용을 다 가져와주는 애다.

        result1.setMsg(ResultCode.SUCCESS.getMsg());
        result1.setCode(ResultCode.SUCCESS.getCode());
        result1.setTotalCount(comments.getTotalElements());
        result1.setList(result);
        result1.setTotalPage(comments.getTotalPages());
        result1.setCurrentPage(comments.getPageable().getPageNumber()+1);

        return result1;
    }
}


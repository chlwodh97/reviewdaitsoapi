package com.daitso.reviewdaitsoapi.service;

import com.daitso.reviewdaitsoapi.entity.Goods;
import com.daitso.reviewdaitsoapi.lib.CommonFile;
import com.daitso.reviewdaitsoapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
@RequiredArgsConstructor
public class TempService {
    private final GoodsRepository goodsRepository;



    public void setGoodsByFile(MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multpartToFile(multipartFile);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 버퍼에서 넘어온 string 한 줄을 임시로 담아 둘 변수가 필요하다.
        String line = "";
        // 버퍼에선 번호를 줄 필요가 없기 때문에
        // 줄 번호 수동 체크를 위해 int로 줄 번호 1씩 증가해서 기록해 둘 변수가 필요함.
        int index = 0;
        // 버퍼에선 개수를 모르기 때문에 while 반복문 사용

        // != null -> 없을 때 까지
        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                //System.out.println(line);
                //System.out.println(index);
                String[] cols = line.split(",");
                if (cols.length == 4) {
//                    System.out.println(cols[0]);
//                    System.out.println(cols[1]);
//                    System.out.println(cols[2]);

                    //이제 db에 넣음 -> C

                    Goods addData = new Goods();
                    addData.setId(Long.parseLong(cols[0]));
                    addData.setGoodsName(cols[1]);
                    addData.setGoodsPrice(Double.parseDouble(cols[2]));


                    goodsRepository.save(addData);
                }
                index++;
                //자기 자신에 +1
                // index 는 몇번 째 줄인지 표시 + 0번째 데이터 안보이기 위해
            }
            bufferedReader.close();
            // 버퍼는 다 읽고 나서 닫아줘야함
        }
    }
}

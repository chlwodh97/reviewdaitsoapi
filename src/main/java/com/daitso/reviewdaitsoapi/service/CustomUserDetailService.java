package com.daitso.reviewdaitsoapi.service;


import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailService implements UserDetailsService {
    private final MemberRepository memberRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Member member = memberRepository.findByUsername(username).orElseThrow();
        // orElseThrow() 여기 안에 예외처리 메시지 가능함

        //Member가 UserDetail을 implements(구현) 하고 있기 때문에
        //리턴타입과 달라도 상관없다
        return member;

        // return memberRepository.findByUsername(username);
        // 로 한 줄로 줄이기 가능.
    }
}

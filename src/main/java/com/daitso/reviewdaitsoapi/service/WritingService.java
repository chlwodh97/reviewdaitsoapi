package com.daitso.reviewdaitsoapi.service;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Goods;
import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.enums.ResultCode;
import com.daitso.reviewdaitsoapi.enums.SortType;
import com.daitso.reviewdaitsoapi.model.category.CategoryItem;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.writing.*;
import com.daitso.reviewdaitsoapi.repository.WritingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class WritingService {
    private final WritingRepository writingRepository;

    public Writing getData(long id) {
        return writingRepository.findById(id).orElseThrow();
    }


    public void setWriting(WritingRequest request, Member member, Category category, Goods goods) {

        Writing addData = new Writing();
        addData.setMember(member);
        addData.setCategory(category);
        addData.setGoods(goods);

        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());
        addData.setDateCreate(LocalDateTime.now());
        addData.setImgUrl(request.getImgUrl());
        addData.setViewNumber(0);
        addData.setHitNumber(0);
        addData.setCommentNumber(0);

        writingRepository.save(addData);
    }

    public List<WritingItem> getWritings() {
        List<Writing> originList = writingRepository.findAll();


        List<WritingItem> result = new LinkedList<>();

        for (Writing writing : originList) {
            WritingItem addItem = new WritingItem();

            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setGoodsId(writing.getGoods().getId());


            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setDateCreate(writing.getDateCreate());
            addItem.setImgUrl(writing.getImgUrl());

            result.add(addItem);
        }
        return result;
    }

    public WritingResponse getWriting(long id) {
        Writing originData = writingRepository.findById(id).orElseThrow();


        originData.setViewNumber(originData.getViewNumber() + 1);

        writingRepository.save(originData);

        WritingResponse response = new WritingResponse();
        response.setId(originData.getId());

        response.setMemberId(originData.getMember().getId());
        response.setNickName(originData.getMember().getNickName());
        response.setIsAdmin(originData.getMember().getIsAdmin() ? "관리자" : "회원");

        response.setCategoryId(originData.getCategory().getId());
        response.setCategoryName(originData.getCategory().getName());

        response.setGoodsId(originData.getGoods().getId());

        response.setTitle(originData.getTitle());
        response.setContent(originData.getContent());
        response.setViewNumber(originData.getViewNumber());
        response.setHitNumber(originData.getHitNumber());
        response.setCommentNumber(originData.getCommentNumber());
        response.setDateCreate(LocalDateTime.now());
        response.setImgUrl(originData.getImgUrl());

        return response;
    }

    public void putWritingTitle(long id, WritingChangeRequest request) {
        Writing originData = writingRepository.findById(id).orElseThrow();
        originData.setTitle(request.getTitle());
        originData.setContent(request.getContent());

        writingRepository.save(originData);
    }


    /**
     * 타이틀 키워드 검색을 하면 조인내용 다 가져오니까
     * 새로운 그릇으로 내가 원하는 것만 뽑기
     */
    public List<WritingSearch> searchTitle(String keyword) {
        List<Writing> originList = writingRepository.findByTitleContaining(keyword);

        List<WritingSearch> result = new LinkedList<>();

        for (Writing writing : originList) {
            WritingSearch addItem = new WritingSearch();


            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 최신순 정렬
     */
    public List<WritingSearch> getListOreder() {
        List<Writing> originList = writingRepository.findAllByOrderByIdDesc();

        List<WritingSearch> result = new LinkedList<>();

        for (Writing writing : originList) {
            WritingSearch addItem = new WritingSearch();
            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());

            addItem.setGoodsId(writing.getGoods().getId());


            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 컨텐트 키워드 검색
     */

    public List<WritingSearch> searchContent(String keyword) {
        List<Writing> originList = writingRepository.findByContentContaining(keyword);

        List<WritingSearch> result = new LinkedList<>();

        for (Writing writing : originList) {
            WritingSearch addItem = new WritingSearch();


            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setGoodsId(writing.getGoods().getId());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");


            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 게시글 중 id 에 해당하는 글 리스트
     */
    public List<WritingSearchId> searchId(long id) {
        List<Writing> originList = writingRepository.findByMember_Id(id);

        List<WritingSearchId> result = new LinkedList<>();

        for (Writing writing : originList) {

            WritingSearchId addItem = new WritingSearchId();
            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setGoodsId(writing.getGoods().getId());

            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 조회수 순 보기
     */

    public List<WritingSearch> viewNumberList() {
        List<Writing> originList = writingRepository.findAllByOrderByViewNumberDesc();

        List<WritingSearch> result = new LinkedList<>();

        for (Writing writing : originList) {
            WritingSearch addItem = new WritingSearch();
            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setGoodsId(writing.getGoods().getId());


            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());

            result.add(addItem);
        }
        return result;
    }

    /**
     * 추천순 보기
     */
    public List<WritingSearch> viewHitList() {
        List<Writing> originList = writingRepository.findAllByOrderByHitNumberDesc();

        List<WritingSearch> result = new LinkedList<>();

        for (Writing writing : originList) {
            WritingSearch addItem = new WritingSearch();
            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setGoodsId(writing.getGoods().getId());


            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());

            result.add(addItem);

        }
        return result;
    }

    /**
     * QR에 필요한 굿즈 번호 게시글 검색 (GoodsId 검색)
     */
    public List<WritingSearch> getGoodsNumberList(long id) {
        List<Writing> originData = writingRepository.findByGoods_Id(id);

        List<WritingSearch> result = new LinkedList<>();

        for (Writing writing : originData) {
            WritingSearch addItem = new WritingSearch();
            addItem.setMemberId(writing.getMember().getId());
            addItem.setNickName(writing.getMember().getNickName());
            addItem.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addItem.setCategoryId(writing.getCategory().getId());
            addItem.setCategoryName(writing.getCategory().getName());
            addItem.setGoodsId(writing.getGoods().getId());


            addItem.setId(writing.getId());
            addItem.setTitle(writing.getTitle());
            addItem.setContent(writing.getContent());
            addItem.setCommentNumber(writing.getCommentNumber());
            addItem.setViewNumber(writing.getViewNumber());
            addItem.setHitNumber(writing.getHitNumber());

            result.add(addItem);
        }
        return result;

    }

    /**
     * 게시글 최신순 페이징
     */
    public ListResult<WritingItem> getLatestWritingsPage(int pageNum) {

        PageRequest pageRequest = PageRequest.of(pageNum - 1, 10);

        Page<Writing> writings = writingRepository.findAllByOrderByIdDesc(pageRequest);

        List<WritingItem> result = new LinkedList<>();

        for (Writing writing : writings.getContent()) {
            WritingItem addData = new WritingItem();
            addData.setId(writing.getId());
            addData.setTitle(writing.getTitle());
            addData.setContent(writing.getContent());
            addData.setViewNumber(writing.getViewNumber());
            addData.setHitNumber(writing.getHitNumber());
            addData.setCommentNumber(writing.getCommentNumber());
            addData.setDateCreate(writing.getDateCreate());
            addData.setImgUrl(writing.getImgUrl());


            addData.setMemberId(writing.getMember().getId());
            addData.setNickName(writing.getMember().getNickName());
            addData.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addData.setCategoryId(writing.getCategory().getId());
            addData.setCategoryName(writing.getCategory().getName());

            addData.setGoodsId(writing.getGoods().getId());

            result.add(addData);
        }
        ListResult<WritingItem> result1 = new ListResult<>();

        result1.setMsg(ResultCode.SUCCESS.getMsg());
        result1.setCode(ResultCode.SUCCESS.getCode());
        result1.setList(result);
        result1.setTotalCount(writings.getTotalElements());
        result1.setTotalPage(writings.getTotalPages());
        result1.setCurrentPage(writings.getPageable().getPageNumber() + 1);

        return result1;
    }


    /**
     * Sort 와 Enum 으로 3개 한 번에 받기
     */

    public ListResult<WritingItem> getAllPage(int pageNum, SortType sortType) {

        PageRequest pageRequest = PageRequest.of(pageNum - 1, 10, Sort.by(sortType.getDirection(), sortType.getColumnName()));
        // Sort.by(Enum 타입안에 있는 디렉션 , enum타입 안에있는 ColumnName 가져오기)


//       List<Writing> writingList = writingRepository.findAll(Sort.by(Sort.Direction.DESC, "hitNumber"));

        Page<Writing> writings = writingRepository.findAll(pageRequest);

        List<WritingItem> result = new LinkedList<>();

        for (Writing writing : writings.getContent()) {
            WritingItem addData = new WritingItem();
            addData.setId(writing.getId());
            addData.setTitle(writing.getTitle());
            addData.setContent(writing.getContent());
            addData.setViewNumber(writing.getViewNumber());
            addData.setHitNumber(writing.getHitNumber());
            addData.setCommentNumber(writing.getCommentNumber());
            addData.setDateCreate(writing.getDateCreate());

            addData.setMemberId(writing.getMember().getId());
            addData.setNickName(writing.getMember().getNickName());
            addData.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addData.setCategoryId(writing.getCategory().getId());
            addData.setCategoryName(writing.getCategory().getName());

            addData.setGoodsId(writing.getGoods().getId());

            result.add(addData);
        }
        ListResult<WritingItem> response = new ListResult<>();
        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());
        response.setList(result);
        response.setTotalCount(writings.getTotalElements());
        response.setTotalPage(writings.getTotalPages());
        response.setCurrentPage(writings.getPageable().getPageNumber() + 1);

        return response;
    }

    /**
     * (페이징)게시글 중 id 에 해당하는 글 리스트
     */
    public ListResult<WritingItem> getMemberIdPage(int pageNum, long id) {

        PageRequest pageRequest = PageRequest.of(pageNum - 1, 10);
        // Sort.by(Enum 타입안에 있는 디렉션 , enum타입 안에있는 ColumnName 가져오기)

        Page<Writing> writings = writingRepository.findByMember_Id(pageRequest, id);

        List<WritingItem> result = new LinkedList<>();

        for (Writing writing : writings.getContent()) {
            WritingItem addData = new WritingItem();
            addData.setId(writing.getId());
            addData.setTitle(writing.getTitle());
            addData.setContent(writing.getContent());
            addData.setViewNumber(writing.getViewNumber());
            addData.setHitNumber(writing.getHitNumber());
            addData.setCommentNumber(writing.getCommentNumber());
            addData.setDateCreate(writing.getDateCreate());

            addData.setMemberId(writing.getMember().getId());
            addData.setNickName(writing.getMember().getNickName());
            addData.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addData.setCategoryId(writing.getCategory().getId());
            addData.setCategoryName(writing.getCategory().getName());

            addData.setGoodsId(writing.getGoods().getId());

            result.add(addData);
        }
        ListResult<WritingItem> response = new ListResult<>();

        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());
        response.setList(result);
        response.setTotalCount(writings.getTotalElements());
        response.setTotalPage(writings.getTotalPages());
        response.setCurrentPage(writings.getPageable().getPageNumber() + 1);

        return response;

    }

    /**
     * (페이징)게시글 중 굿즈id 에 해당하는 글 리스트
     */
    public ListResult<WritingItem> getGoodsIdPage(int pageNum, long id) {

        PageRequest pageRequest = PageRequest.of(pageNum - 1, 10);
        // Sort.by(Enum 타입안에 있는 디렉션 , enum타입 안에있는 ColumnName 가져오기)

        Page<Writing> writings = writingRepository.findByGoods_Id(pageRequest, id);

        List<WritingItem> result = new LinkedList<>();

        for (Writing writing : writings.getContent()) {
            WritingItem addData = new WritingItem();
            addData.setId(writing.getId());
            addData.setTitle(writing.getTitle());
            addData.setContent(writing.getContent());
            addData.setViewNumber(writing.getViewNumber());
            addData.setHitNumber(writing.getHitNumber());
            addData.setCommentNumber(writing.getCommentNumber());
            addData.setDateCreate(writing.getDateCreate());

            addData.setMemberId(writing.getMember().getId());
            addData.setNickName(writing.getMember().getNickName());
            addData.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addData.setCategoryId(writing.getCategory().getId());
            addData.setCategoryName(writing.getCategory().getName());

            addData.setGoodsId(writing.getGoods().getId());

            result.add(addData);
        }
        ListResult<WritingItem> response = new ListResult<>();

        response.setCode(ResultCode.SUCCESS.getCode());
        response.setMsg(ResultCode.SUCCESS.getMsg());
        response.setList(result);
        response.setTotalCount(writings.getTotalElements());
        response.setTotalPage(writings.getTotalPages());
        response.setCurrentPage(writings.getPageable().getPageNumber() + 1);

        return response;

    }

    /**
     * 카테고리 id 별 페이징
     */
    public ListResult<WritingItem> getCategoryWritings(long id , int pageNum) {

        PageRequest pageRequest = PageRequest.of(pageNum -1 , 10);

        Page<Writing> writings = writingRepository.findByCategory_Id(pageRequest,id);

        List<WritingItem> result = new LinkedList<>();

        for (Writing writing : writings.getContent()) {
            WritingItem addData = new WritingItem();
            addData.setId(writing.getId());
            addData.setTitle(writing.getTitle());
            addData.setContent(writing.getContent());
            addData.setViewNumber(writing.getViewNumber());
            addData.setHitNumber(writing.getHitNumber());
            addData.setCommentNumber(writing.getCommentNumber());
            addData.setDateCreate(writing.getDateCreate());
            addData.setImgUrl(writing.getImgUrl());

            addData.setMemberId(writing.getMember().getId());
            addData.setNickName(writing.getMember().getNickName());
            addData.setIsAdmin(writing.getMember().getIsAdmin() ? "관리자" : "회원");

            addData.setCategoryId(writing.getCategory().getId());
            addData.setCategoryName(writing.getCategory().getName());

            addData.setGoodsId(writing.getGoods().getId());

            result.add(addData);
        }
        ListResult<WritingItem> result1 = new ListResult<>();
        //getContent 라는 애가 그냥 List의 내용을 다 가져와주는 애다.

        result1.setMsg(ResultCode.SUCCESS.getMsg());
        result1.setCode(ResultCode.SUCCESS.getCode());
        result1.setTotalCount(writings.getTotalElements());
        result1.setList(result);
        result1.setTotalPage(writings.getTotalPages());
        result1.setCurrentPage(writings.getPageable().getPageNumber()+1);

        return result1;

    }
}




package com.daitso.reviewdaitsoapi.service;


import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.model.member.LoginRequest;
import com.daitso.reviewdaitsoapi.model.member.LoginResponse;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.repository.MemberRepository;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;

    public SingleResult<LoginResponse> doLogin(LoginRequest request) {
        // 아이디와 비밀번호가 일치하는 데이터를 가져온다 (암호회를 배우지않아서)

         Optional<Member> member = memberRepository.findByHomeIdAndPassword(
                 request.getHomeId(),
                 request.getPassword()
         );
         // 회원 데이터는 있을 수도 있고 없을 수도 있다
         // 만약에 회원 데이터가 있으면 LoginResponse 채워서 준다
        if (member.isPresent()) {
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.setUserId(member.get().getId());
            loginResponse.setNickName(member.get().getNickName());

            SingleResult<LoginResponse> result = new SingleResult<>();
            result.setData(loginResponse);
            result.setCode(0);
            result.setMsg("로그인 성공");

            return result;

        } else { // 회원데이터가 없으면
            SingleResult<LoginResponse> result = new SingleResult<>();
            result.setCode(-1);
            result.setMsg("아이디나 패스워드를 확인해주세요.");

            return result;
        }
        // 근데 없으면 ..? 메시지를 예쁘게 띄운다 .. 던지면 안댐 에러남 -> 그래서 옵셔널
    }

}

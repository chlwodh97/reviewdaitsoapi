package com.daitso.reviewdaitsoapi.service;


import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Goods;
import com.daitso.reviewdaitsoapi.enums.ResultCode;
import com.daitso.reviewdaitsoapi.model.goods.*;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.writing.WritingItem;
import com.daitso.reviewdaitsoapi.repository.GoodsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;


    public Goods getData(long id) {
        return goodsRepository.findById(id).orElseThrow();
    }

    public void setGoods(GoodsRequest request , Category category) {

        Goods addData = new Goods();
        addData.setCategory(category);
        addData.setId(request.getId());
        addData.setGoodsName(request.getGoodsName());
        addData.setGoodsPrice(request.getGoodsPrice());

        goodsRepository.save(addData);
    }

    public List<GoodsItem> getGoodses(){

        List<Goods> originData = goodsRepository.findAll();

        List<GoodsItem> result = new LinkedList<>();

        for (Goods goods:originData){
            GoodsItem addItem = new GoodsItem();
            addItem.setCategoryId(goods.getCategory().getId());
            addItem.setCategoryName(goods.getCategory().getName());

            addItem.setId(goods.getId());
            addItem.setGoodsName(goods.getGoodsName());
            addItem.setGoodsPrice(goods.getGoodsPrice());


            result.add(addItem);
        }
        return result;
    }

    public GoodsResponse getGoods(long id) {
        Goods originData = goodsRepository.findById(id).orElseThrow();

        GoodsResponse response = new GoodsResponse();
        response.setCategoryId(originData.getCategory().getId());
        response.setCategoryName(originData.getCategory().getName());

        response.setId(originData.getId());
        response.setGoodsName(originData.getGoodsName());
        response.setGoodsPrice(originData.getGoodsPrice());

        return response;
    }

    public void putGoodsName(long id , GoodsNameChangeRequest request) {
        Goods goods = goodsRepository.findById(id).orElseThrow();
        goods.setGoodsName(request.getGoodsName());
    }

    public void putGoodsPrice(long id , GoodsPriceChangeRequest request){
        Goods goods = goodsRepository.findById(id).orElseThrow();
        goods.setGoodsPrice(request.getGoodsPrice());
    }

    public void delGoods(long id) {
        goodsRepository.deleteById(id);
    }



    public ListResult<GoodsItem> getGoodsPage(int pageNum){

        PageRequest pageRequest = PageRequest.of(pageNum -1, 10);
        Page<Goods> goodsPage = goodsRepository.findAll(pageRequest);

        List<GoodsItem> result = new LinkedList<>();

        for (Goods goods:goodsPage.getContent()){
            GoodsItem addItem = new GoodsItem();
            addItem.setCategoryId(goods.getCategory().getId());
            addItem.setCategoryName(goods.getCategory().getName());

            addItem.setId(goods.getId());
            addItem.setGoodsName(goods.getGoodsName());
            addItem.setGoodsPrice(goods.getGoodsPrice());


            result.add(addItem);
        }
        ListResult<GoodsItem> result1 = new ListResult<>();
        //getContent 라는 애가 그냥 List의 내용을 다 가져와주는 애다.

        result1.setMsg(ResultCode.SUCCESS.getMsg());
        result1.setCode(ResultCode.SUCCESS.getCode());
        result1.setTotalCount(goodsPage.getTotalElements());
        result1.setList(result);
        result1.setTotalPage(goodsPage.getTotalPages());
        result1.setCurrentPage(goodsPage.getPageable().getPageNumber()+1);

        return result1;
    }
    }




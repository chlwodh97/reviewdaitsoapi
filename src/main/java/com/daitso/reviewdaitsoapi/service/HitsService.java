package com.daitso.reviewdaitsoapi.service;


import com.daitso.reviewdaitsoapi.entity.Hits;
import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.repository.HItsRepository;
import com.daitso.reviewdaitsoapi.repository.WritingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HitsService {
    private final HItsRepository hItsRepository;
    private final WritingRepository writingRepository;


    public void setHits(Member member , Writing writing) {

        Optional<Hits> newHits = hItsRepository.findByMemberAndWriting(member , writing);

        if (newHits.isEmpty()){
            Hits addItem = new Hits();
            addItem.setMember(member);
            addItem.setWriting(writing);

            hItsRepository.save(addItem);

            writing.setHitNumber(writing.getHitNumber()+1);
            writingRepository.save(writing);

            // 레포지토리한테 세팅할 때 해당 게시글 추천수 올라가게

        } else {
            hItsRepository.delete(newHits.get());
            // Optional 로 가져오면 있을수도 있고 없을수도 있다. .get()하면 있을때 그 모양 엔티티 가져온다.
            // 삭제를 newHits 에서 값을 받고 else(있으면) 삭제
            // else (있으면) 삭제해라 (newHits에서 받은 값을)
            writing.setHitNumber(writing.getHitNumber()-1);
            writingRepository.save(writing);

        }

    }
}

package com.daitso.reviewdaitsoapi.service;


import com.daitso.reviewdaitsoapi.entity.Comment;
import com.daitso.reviewdaitsoapi.entity.Hits;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.repository.CommentRepository;
import com.daitso.reviewdaitsoapi.repository.HItsRepository;
import com.daitso.reviewdaitsoapi.repository.WritingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteService {
    private final HItsRepository hItsRepository;
    private final CommentRepository commentRepository;
    private final WritingRepository writingRepository;

    public void delWriting(long id){

        List<Hits> hits = hItsRepository.findByWritingId(id);

        for (Hits hits1 : hits) {
            hItsRepository.deleteById(hits1.getId());
        }

        List<Comment> comments = commentRepository.findByWritingId(id);

        for (Comment comment1 : comments) {
            commentRepository.deleteById(comment1.getId());
        }

        writingRepository.deleteById(id);
    }
}

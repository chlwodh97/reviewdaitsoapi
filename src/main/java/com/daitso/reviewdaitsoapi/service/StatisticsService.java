package com.daitso.reviewdaitsoapi.service;


import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.model.NearDayStatistics.NearDayStatisticsResponse;
import com.daitso.reviewdaitsoapi.repository.MemberRepository;
import com.daitso.reviewdaitsoapi.repository.WritingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StatisticsService {
    private final MemberRepository memberRepository;
    private final WritingRepository writingRepository;

    /**
     * 최근 7일 회원가입자 수 구하기
     */
    public NearDayStatisticsResponse getNearDayJoinStatistics() {
        LocalDateTime today = LocalDateTime.now();
        // today 에 지금 시간 넣기
        LocalDateTime startDay = today.minusDays(6);
        // 지금 시간 에서 -6 일을 하니 최근 7일이 된다

        NearDayStatisticsResponse response = new NearDayStatisticsResponse();

        List<String> labels = new LinkedList<>();
        List<Long> datasets = new LinkedList<>();

        int i = 0;

        for (i = 0; i <= 6; i++) {

            String LocalDateForm = startDay.plusDays(i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            // 데이터를 2024-02-17T11:45:50.605258718" 형식 말고 잘라서 줘야하니까 for문 안에 따로 선언

            LocalDateTime startTime = LocalDateTime.of(
                    startDay.plusDays(i).getYear(),
                    startDay.plusDays(i).getMonth(),
                    startDay.plusDays(i).getDayOfMonth(),
                    0,
                    0,
                    0
            );
            // 스타트 타임은 스타트 데이에서 년, 월, 일 을 가져오고 나머지 값 시, 분, 초는 0 0 0 으로 채워줌
            LocalDateTime endTime = LocalDateTime.of(
                    startDay.plusDays(i).getYear(),
                    startDay.plusDays(i).getMonth(),
                    startDay.plusDays(i).getDayOfMonth(),
                    23,
                    59,
                    59
            );
            // 비트윈은 시작~끝 지점 사이를 뜻하니 시작 , 끝을 만들어줌 -> 시작일 기준 하루 0시0분0초 부터 23시59분 59초 까지
            labels.add(LocalDateForm);

            datasets.add(memberRepository.countByDateJoinBetween(startTime , endTime));

            // 레포지토리 에서 날짜세기 하는데 . 시작일로 부터 plus i 만큼세기
        }
        // 라벨과 데이트셑에 1일~ 2일~ 3일~ 4일~ 반복 해야하니까 for 문으로 돌리기


        response.setLabels(labels);
        response.setDatasets(datasets);

        return response;

    }

    /**
     * 최근 7일 게시글 수 구하기
     */
    public NearDayStatisticsResponse getNearDayWritingStatistics() {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime startDay = today.minusDays(6);

        NearDayStatisticsResponse response = new NearDayStatisticsResponse();

        List<String> labels = new LinkedList<>();
        List<Long> datasets = new LinkedList<>();

        int i = 0;

        for (i = 0; i <= 6; i++){
            String LocalDateForm = startDay.plusDays(i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

            LocalDateTime startTime = LocalDateTime.of(
                    startDay.plusDays(i).getYear(),
                    startDay.plusDays(i).getMonth(),
                    startDay.plusDays(i).getDayOfMonth(),
                    0,
                    0,
                    0
            );

            LocalDateTime endTime = LocalDateTime.of(
                    startDay.plusDays(i).getYear(),
                    startDay.plusDays(i).getMonth(),
                    startDay.plusDays(i).getDayOfMonth(),
                    23,
                    59,
                    59
            );
            labels.add(LocalDateForm);

            datasets.add(writingRepository.countByDateCreateBetween(startTime , endTime));
        }

        response.setLabels(labels);
        response.setDatasets(datasets);

        return response;
    }



}

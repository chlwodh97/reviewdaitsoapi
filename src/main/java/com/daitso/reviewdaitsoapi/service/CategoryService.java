package com.daitso.reviewdaitsoapi.service;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.enums.ResultCode;
import com.daitso.reviewdaitsoapi.model.category.CategoryItem;
import com.daitso.reviewdaitsoapi.model.category.CategoryNameChangeRequest;
import com.daitso.reviewdaitsoapi.model.category.CategoryRequest;
import com.daitso.reviewdaitsoapi.model.category.CategoryResponse;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public Category getData(long id){
        return categoryRepository.findById(id).orElseThrow();

    }

    public void setCategory(CategoryRequest request) {
        Category addData = new Category();
        addData.setName(request.getName());
        addData.setImgUrl(request.getImgUrl());
        addData.setCategoryType(request.getCategoryType());
        addData.setBigCategory(request.getBigCategory());
        addData.setEtcMemo(request.getEtcMemo());

        categoryRepository.save(addData);
    }



    public List<CategoryItem> getCategorys() {
        List<Category> originList = categoryRepository.findAll();

        List<CategoryItem> result = new LinkedList<>();
        for (Category category : originList){
            CategoryItem addItem = new CategoryItem();
            addItem.setId(category.getId());
            addItem.setName(category.getName());
            addItem.setImgUrl(category.getImgUrl());
            addItem.setCategoryType(category.getCategoryType());
            addItem.setBigCategory(category.getBigCategory());
            addItem.setEtcMemo(category.getEtcMemo());

            result.add(addItem);
        }
        return result;
    }

    public void putCategoryName(long id, CategoryNameChangeRequest request) {
        Category originData = categoryRepository.findById(id).orElseThrow();
        originData.setName(request.getName());

        categoryRepository.save(originData);
    }

    public void delCategory(long id) {
        categoryRepository.deleteById(id);
    }

    /**
     * 카테고리 페이징
     */
    public ListResult<CategoryItem> getCategorysPage(int pageNum) {

        PageRequest pageRequest = PageRequest.of(pageNum -1 , 10, Sort.by(Sort.Direction.ASC , "id"));

        Page<Category> categories = categoryRepository.findAll(pageRequest);

        List<CategoryItem> result = new LinkedList<>();
        for (Category category : categories.getContent()){
            CategoryItem addItem = new CategoryItem();
            addItem.setId(category.getId());
            addItem.setName(category.getName());
            addItem.setImgUrl(category.getImgUrl());
            addItem.setCategoryType(category.getCategoryType());
            addItem.setBigCategory(category.getBigCategory());
            addItem.setEtcMemo(category.getEtcMemo());

            result.add(addItem);
        }
        ListResult<CategoryItem> result1 = new ListResult<>();
        //getContent 라는 애가 그냥 List의 내용을 다 가져와주는 애다.

        result1.setMsg(ResultCode.SUCCESS.getMsg());
        result1.setCode(ResultCode.SUCCESS.getCode());
        result1.setTotalCount(categories.getTotalElements());
        result1.setList(result);
        result1.setTotalPage(categories.getTotalPages());
        result1.setCurrentPage(categories.getPageable().getPageNumber()+1);

        return result1;
    }

    public CategoryResponse getCategory(long id){

        Category originData = categoryRepository.findById(id).orElseThrow();

        CategoryResponse response = new CategoryResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setCategoryType(originData.getCategoryType());
        response.setBigCategory(originData.getBigCategory());
        response.setImgUrl(originData.getImgUrl());
        response.setEtcMemo(originData.getEtcMemo());

        return response;

    }
}

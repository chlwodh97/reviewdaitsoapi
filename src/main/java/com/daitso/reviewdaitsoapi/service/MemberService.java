package com.daitso.reviewdaitsoapi.service;

import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.enums.MemberLevel;
import com.daitso.reviewdaitsoapi.enums.MemberStatus;
import com.daitso.reviewdaitsoapi.enums.ResultCode;
import com.daitso.reviewdaitsoapi.model.member.*;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();

    }

    public void setMember(MemberRequest request) {
        Member addData = new Member();
        addData.setHomeId(request.getHomeId());
        addData.setPassword(request.getPassword());
        addData.setNickName(request.getNickName());
        addData.setStatus(MemberStatus.NORMAL);
        addData.setDateJoin(LocalDateTime.now());
        addData.setLevel(MemberLevel.SILVER);
        addData.setIsAdmin(false);

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setHomeId(member.getHomeId());
            addItem.setNickName(member.getNickName());
            addItem.setDateJoin(member.getDateJoin());
            addItem.setStatus(member.getStatus());
            addItem.setLevel(member.getLevel());
            addItem.setIsAdmin(member.getIsAdmin()? "관리자":"회원");

            result.add(addItem);
        }

        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setId(originData.getId());
        response.setHomeId(originData.getHomeId());
        response.setPassword(originData.getPassword());
        response.setNickName(originData.getNickName());
        response.setStatus(originData.getStatus());
        response.setDateJoin(originData.getDateJoin());
        response.setLevel(originData.getLevel());
        response.setIsAdmin(originData.getIsAdmin());

        return response;
    }

    public void putMemberStatus(long id, MemberChangeStatusRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setStatus(request.getStatus());

        memberRepository.save(originData);
    }

    public void putMemberLevel(long id, MemberChangeLevelRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setLevel(request.getLevel());

        memberRepository.save(originData);
    }

    public void delMember(long id) {
        memberRepository.deleteById(id);
    }


    /**
     * 페이징 - 10개씩 1 페이지
     */
    public ListResult<MemberItem> getPageMembers(int pageNum){
        PageRequest pageRequest = PageRequest.of(pageNum -1 , 10, Sort.by(Sort.Direction.ASC , "id"));
        // 컴퓨터는 0부터 시작이니까 pageNum에 -1 을 해준다

        Page<Member> members = memberRepository.findAll(pageRequest);

        List<MemberItem> result = new LinkedList<>();

        for (Member member : members.getContent()) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setHomeId(member.getHomeId());
            addItem.setNickName(member.getNickName());
            addItem.setDateJoin(member.getDateJoin());
            addItem.setStatus(member.getStatus());
            addItem.setLevel(member.getLevel());
            addItem.setIsAdmin(member.getIsAdmin()? "관리자":"회원");

            result.add(addItem);
        }

        ListResult<MemberItem> result1 = new ListResult<>();
        //getContent 라는 애가 그냥 List의 내용을 다 가져와주는 애다.

        result1.setMsg(ResultCode.SUCCESS.getMsg());
        result1.setCode(ResultCode.SUCCESS.getCode());
        result1.setTotalCount(members.getTotalElements());
        result1.setList(result);
        result1.setTotalPage(members.getTotalPages());
        result1.setCurrentPage(members.getPageable().getPageNumber()+1);

        return result1;
    }


}

package com.daitso.reviewdaitsoapi.lib;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class CommonFile {

    /**
     * FIle 은 io (input , output) 이 붙어 있는 것
     * getProperty("java.io.tmpdir")); -> 프로젝트의 root가 잡힘
     * "/" + multipartFile.getOriginalFilename());  -> + / + 파일 이름
     */
    public static File multpartToFile(MultipartFile multipartFile) throws IOException {

        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + multipartFile.getOriginalFilename());
        // 멀티파트 파일을 변환해서 to convFile 에 넣으세용
        multipartFile.transferTo(convFile);
        // 트랜스퍼투 = 오버로딩 같은 느낌
        return convFile;
    }
}

package com.daitso.reviewdaitsoapi.configure;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

    @PostConstruct // Post 먼저 // Construct 구조
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
        //Base64 = 옛날 암호화 기법 단방향 방식
    }

    //토큰 생성
    // claim 이라는 단어 뜻 찾아보기.
    // claim -> "나야" 하는거
    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("role", role); // role -> 역할
        Date now = new Date(); // 외국에서 할 수도 있음
        // 토큰 유효시간
        // 1000 밀리세컨드 = 1초
        // 기본으로 10시간 유효하게 설정해 줌, 왜? -> 아침에 출근해서 로그인하고 점심먹고 퇴근하면 대충 10시간 이니까.
        // 앱용 토큰 같은 경우 유효시간 1년으로 설정해 줌. 앱에서 아침마다 로그인하라고 하면 짜증나니까.

        long tokenValidMillisecond = 1000L * 60 * 60 * 10; // 10시간
        if (type.equals("APP")) tokenValidMillisecond = 1000L * 60 * 60 * 24 * 365; // 1년
        // 토큰 생성해서 리턴
        // jwt 사이트 참고.
        // 유효시간도 넣어줌. 생성요청한 시간 ~ 현재 + 위에서 설정된 유효초 만큼
        // 웹 토큰 쿠키의 시간도 동일하게 맞춰야한다.
        return Jwts.builder()
                .setClaims(claims) // 나야 세팅
                .setIssuedAt(now) // 언제 발급됬다
                .setExpiration(new Date(now.getTime() + tokenValidMillisecond)) // 만료일 ~부터~까지 유효한 토큰
                .signWith(SignatureAlgorithm.ES256, secretKey) // 위조방지를 위한 사인
                .compact();

        // 만들어서 리턴하면 스트링으로 토큰 준다
    }

    // 토큰을 분석하여 인증정보를 가져옴
    // Authentication 일회용 출입증
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    // 토큰을 파싱하여 username을 가져옴.
    // 토큰 생성시 username은 subject에 넣은 것 꼭 확인.
    // jwt 사이트 보면서 코드 이해하기.

    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }
    // 리졸브(resolve(결정,해결))라는 단어도 많이 씀 . 단어 뜻 검색
    // 프론트가 다른서버에 api 호출 했을 때 전화걸고 기다려야함
    // -> 실패하든 성공하든 프로미스다 .. 리졸브는 프로미스 중에서 성공한 것만 리졸브라고 한다..

    // resolveToken 성공한토큰을 가져옴
    // 요청이 성공해야만 헤더에서 토큰값을 가져올 수 있다..
    public String resolveToken(HttpServletRequest request) {
        // rest api - header 인증 방식에서 Bearer 를 언제 사용하는지 보기.
        return request.getHeader(HttpHeaders.AUTHORIZATION); //AUTHORIZATION
    }

    //유효시간을 검사하여 유효시간이 지났으면 false를 줌.
    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}

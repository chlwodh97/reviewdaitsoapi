package com.daitso.reviewdaitsoapi.configure;


import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final JwtTokenProvider jwtTokenProvider;
    private final CustomAuthenticationEntryPoint entryPoint;
    private final CustomAccessDeniedHandler accessDenied;

    @Bean   //CORS 구성 소스
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();

        // 인증정보허용(트루)
        config.setAllowCredentials(true);
        // 허용된 원산지
        config.setAllowedOrigins(List.of("*"));
        // 허용되는 방법 // 옵션은 Api 호출시
        config.setAllowedMethods(List.of("GET", "POST","PUT","DELETE","PATCH","OPTIONS"));
        // 허용된 헤더
        config.setAllowedHeaders(List.of("*"));
        // 노출된 해더
        config.setExposedHeaders(List.of("*"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // 구성 등록 "/**" -> 모든 매핑 주소에 윗 설정들을 적용하겠다
        source.registerCorsConfiguration("/**",config);
        return source;
    }
    @Bean
    protected SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.csrf(AbstractHttpConfigurer::disable) //disable 비활성화
                .formLogin(AbstractHttpConfigurer::disable)
                .httpBasic((AbstractHttpConfigurer::disable))
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSource()))
                // 설정을 시큐리티 체인으로 묶는다..
                .sessionManagement((sessionManagement)->
                        sessionManagement.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                //authorize 는 인가 -> 인증요청을 허가하는 것 -> 데이터 요청을 허가하는
                .authorizeHttpRequests((authorizeRequests) ->
                        authorizeRequests
                                // Matchers 주소와 매칭 되는 것
                                // 일단 예외처리는 다 풀어준다
                                .requestMatchers("/v3/**", "/swagger-ui/**").permitAll()
                                .requestMatchers(HttpMethod.GET,"/exception/**").permitAll()
                                // 로그인도 다 풀어준다.
                                .requestMatchers("/v1/member/login/**").permitAll()
                                .requestMatchers("/v1/auth-test/test-admin").hasAnyRole("MEMBER","ADMIN")
                                .anyRequest().hasRole("ADMIN")
                );
        // 잘못된 권한이거나 없으면 핸들러한테 에러를 넘긴다.
        http.exceptionHandling(handler -> handler.accessDeniedHandler(accessDenied));
        http.exceptionHandling(handler -> handler.authenticationEntryPoint(entryPoint));

        //필터 비포 수행하기 전에 체이닝 하면서 맨 처음으로..
        http.addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
                                                                    // .class 는 필터의 자리라고 생각하자

        return http.build();
    }
}

package com.daitso.reviewdaitsoapi.configure;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component // 기능 묶음
// 서비스, 컨트롤러도 아닌데 무언가 기능을 하는 단위다 라는 것을 알려줌
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
// 오면 안되는데 들어오는 경우에 대해서 이런 에러가 터지면 어떻게 할 거야
    @Override         //Http -> 데이터 왔다 갔다 ~
                // HttpServletRequest  고객이 서버한테 요청하는 데이터를 서빙
                // HttpServletResponse 서버가 고객한테 주는 데이터를 서빙
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        response.sendRedirect("/exception/access-denied");

        //response.sendRedirect(); 어디로 보낼건지 -> 서버가 고객한테 주는 데이터니까 response
        //익셉션컨트롤러한테 보내주기
    }
}

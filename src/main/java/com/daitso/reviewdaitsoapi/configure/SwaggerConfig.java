package com.daitso.reviewdaitsoapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@OpenAPIDefinition(
        info = @Info(title = "리뷰다있소  App",
                description = "다이소에 있는 물품의 리뷰를 올리는 사이트",
                version = "v1"))

@Configuration
public class SwaggerConfig {
    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("리뷰다있소 API v1")
                .pathsToMatch(paths)
                .build();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList("JWT", new ArrayList<>()))
                .components(new io.swagger.v3.oas.models.Components()
                        .addSecuritySchemes("JWT", new SecurityScheme()
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .bearerFormat("JWT")));
    }
}

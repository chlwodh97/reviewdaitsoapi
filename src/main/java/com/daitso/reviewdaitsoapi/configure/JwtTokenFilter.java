package com.daitso.reviewdaitsoapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;


@Component
@RequiredArgsConstructor
                                    //Bean 시작할 때 같이 가는 애
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;
    @Override                                                  //Chain 필터끼리 엮는다. 필터단계 설정
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        String token = "";
        // 토큰이없거나 다른 방식의 토큰이면
        if (tokenFull == null || tokenFull.startsWith("Bearer ")) {
            chain.doFilter(request,response);
            // 다음 필터로 넘기기 (순서개념이 아님)
            return; // 리턴이 되는 순간 끝나버림 -> 토큰없다.
        }
        // 파싱해서뒤에 토큰값만 가져오기 두번째가 아니라 첫번째 요소다  [1] -> 0베어 / 1토큰값
        token = tokenFull.split("")[1].trim();
        // 토큰 검사 후 유효한 토큰 아니면 나감
        if (!jwtTokenProvider.validateToken(token)) {
            chain.doFilter(request, response);
            return; // 리턴이 되는 순간 끝나버림 -> 토큰없다.
        }
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
                //Context 환경 Holder 다같이 보려고 고정 시킴
        SecurityContextHolder.getContext().setAuthentication(authentication);
                // 일회용 출입증 고정시켜 놓겠다.. 왔다갔다 하면 유실될 수 있으니
                // -> 고정시켜놨으니 보기만하면 사용 가능 ..
        chain.doFilter(request ,response);
    }

}
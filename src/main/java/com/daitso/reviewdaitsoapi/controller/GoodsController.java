package com.daitso.reviewdaitsoapi.controller;


import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Goods;
import com.daitso.reviewdaitsoapi.model.goods.*;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.service.CategoryService;
import com.daitso.reviewdaitsoapi.service.GoodsService;
import com.daitso.reviewdaitsoapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {
    private final GoodsService goodsService;
    private final CategoryService categoryService;


    @PostMapping("register/categoryId/{categoryId}")
    @Operation(summary = "굿즈 등록")
    public CommonResult setGoods(@PathVariable long categoryId , @RequestBody GoodsRequest request){

        Category category = categoryService.getData(categoryId);

        goodsService.setGoods(request, category);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "굿즈 리스트 전체보기")
    public ListResult<GoodsItem> getGoodses() {
        return ResponseService.getListResult(goodsService.getGoodses());
    }

    @GetMapping("/detail/{id}")
    @Operation(summary = "굿즈 상세보기")
    public SingleResult<GoodsResponse> getGoods(@PathVariable long id){

        return ResponseService.getSingleResult(goodsService.getGoods(id));
    }

    @PutMapping("/change-name/{id}")
    @Operation(summary = "굿즈 이름 수정")
    public CommonResult putGoodsName(@PathVariable long id , @RequestBody GoodsNameChangeRequest request) {

        goodsService.putGoodsName(id , request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/change-price/{id}")
    @Operation(summary = "굿즈 가격 수정")
    public CommonResult putGoodsPrice(@PathVariable long id , @RequestBody GoodsPriceChangeRequest request){
        goodsService.putGoodsPrice(id, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "굿즈 삭제")
    public CommonResult delGoods(@PathVariable long id) {
        goodsService.delGoods(id);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "굿즈 페이지")
    public ListResult<GoodsItem> getGoodsPage(@PathVariable int pageNum){

        return goodsService.getGoodsPage(pageNum);
    }

}

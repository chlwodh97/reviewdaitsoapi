package com.daitso.reviewdaitsoapi.controller;



import com.daitso.reviewdaitsoapi.model.NearDayStatistics.NearDayStatisticsResponse;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.service.ResponseService;
import com.daitso.reviewdaitsoapi.service.StatisticsService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/chart")
public class StatisticsController {
    private final StatisticsService statisticsService;

    @GetMapping("/join-member")
    @Operation(summary = "최근 7일간 회원가입 수 보기")
    public SingleResult<NearDayStatisticsResponse> getNearDayJoinStatistics() {

        return ResponseService.getSingleResult(statisticsService.getNearDayJoinStatistics());
    }

    @GetMapping("/join-writing")
    @Operation(summary = "최근 7일간 게시글 수 보기")
    public SingleResult<NearDayStatisticsResponse> getNearDayWritingStatistics() {

        return ResponseService.getSingleResult(statisticsService.getNearDayWritingStatistics());


    }
}

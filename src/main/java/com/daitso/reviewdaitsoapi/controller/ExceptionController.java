package com.daitso.reviewdaitsoapi.controller;


import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException(){
        throw new ClassCastException(); // C만든 예외처리 해주기
        //용도 : 회원들만 가능한 ~ 입니다 의 경우
    }
    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new ClassCastException(); // C만든 예외처리 해주기
        //용도 : 로그인 상태에서 권한이 있는 회원만 사용 가능한 ~ 경우
    }
}


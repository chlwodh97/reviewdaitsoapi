package com.daitso.reviewdaitsoapi.controller;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Goods;
import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.enums.SortType;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.model.writing.*;
import com.daitso.reviewdaitsoapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.RequiredArgsConstructor;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/writing")
public class WritingController {
    private final WritingService writingService;
    private final MemberService memberService;
    private final CategoryService categoryService;
    private final GoodsService goodsService;
    private final DeleteService deleteService;

    @PostMapping("/register/member-id/{memberId}/category-id/{categoryId}/goods-id/{goodsId}")
    @Operation(summary = "게시글 등록")
    public CommonResult setWriting (@PathVariable long memberId , @PathVariable long categoryId ,  @PathVariable long goodsId,@RequestBody WritingRequest request ) {
        Member member = memberService.getData(memberId);
        Category category = categoryService.getData(categoryId);
        Goods goods = goodsService.getData(goodsId);

        writingService.setWriting(request, member , category , goods);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "게시글 리스트 전체보기")
    public ListResult<WritingItem> getWritings() {
        return ResponseService.getListResult(writingService.getWritings());
    }

    @GetMapping("/detail/writing-id/{writingId}")
    @Operation(summary = "게시글 상세보기")
    public SingleResult<WritingResponse> getWriting(@PathVariable long writingId) {
        return ResponseService.getSingleResult(writingService.getWriting(writingId));
    }

    @PutMapping("/title-change-id/{id}")
    @Operation(summary = "게시글 제목 수정")
    public CommonResult putWritingTitle(@PathVariable long id, @RequestBody WritingChangeRequest request) {
        writingService.putWritingTitle(id, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/writing-id/{writingId}")
    @Operation(summary = "게시글 삭제")
    public CommonResult delWriting(@PathVariable long writingId) {
        deleteService.delWriting(writingId);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/search-title")
    @Operation(summary = "게시글 제목 검색")

    public ListResult<WritingSearch> searchTitle(String keyword){

        return ResponseService.getListResult(writingService.searchTitle(keyword));
    }

    @GetMapping("/search-content")
    @Operation(summary = "게시글 내용 검색")
    public ListResult<WritingSearch> searchContent(String keyword){

        return ResponseService.getListResult(writingService.searchContent(keyword));
    }

    @GetMapping("/latest-order")
    @Operation(summary = "게시글 최신순 보기")
    public ListResult<WritingSearch> getListOrder() {
        return ResponseService.getListResult(writingService.getListOreder());
    }

    @GetMapping("/user/search-id/{id}")
    @Operation(summary = "게시글 id 해당하는 글보기")
    public ListResult<WritingSearchId> searchId(@PathVariable long id) {
        return ResponseService.getListResult(writingService.searchId(id));
    }

    @GetMapping("/view-number/list")
    @Operation(summary = "게시글 조회순 보기")
    public ListResult<WritingSearch> viewNumberList() {
        return ResponseService.getListResult(writingService.viewNumberList());
    }

    @GetMapping("/view-hit/list")
    @Operation(summary = "게시글 추천순 보기")
    public ListResult<WritingSearch> viewHitList() {
        return ResponseService.getListResult(writingService.viewHitList());
    }

    @GetMapping("/view-goods-id/{goodsId}")
    @Operation(summary = "굿즈 id 해당하는 게시물 보기")
    public ListResult<WritingSearch> getGoodsNumberList(@PathVariable long goodsId){


        return ResponseService.getListResult(writingService.getGoodsNumberList(goodsId));
    }

    @GetMapping("/new/{pageNum}")
    @Operation(summary = "최신순 게시글 10개당 한 페이지")
    public ListResult<WritingItem> getPageWritings(@PathVariable int pageNum) {

        return writingService.getLatestWritingsPage(pageNum);
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "최신순, 추천순, 조회순 10개당 1페이지")
    public ListResult<WritingItem> getAllPage(@PathVariable int pageNum,
                                             @RequestParam(name = "sortType", required = false, defaultValue = "NEAR" )SortType sortType ) {
        return writingService.getAllPage(pageNum, sortType);
    }

    @GetMapping("/{pageNum}/goods-id/{goodsId}")
    @Operation(summary = "굿즈 id 검색 10개당 한 페이지")
    public ListResult<WritingItem> getGoodsIdPage(@PathVariable int pageNum , @PathVariable long goodsId){

        return writingService.getGoodsIdPage(pageNum, goodsId);
    }

    @GetMapping("/{pageNum}/member-id/{memberId}")
    @Operation(summary = "멤버 id 검색 10개당 한 페이지")
    public ListResult<WritingItem> getMemberIdPage(@PathVariable int pageNum , @PathVariable long memberId) {

        return writingService.getMemberIdPage(pageNum, memberId);

    }

    @GetMapping("/category/{categoryId}/{pageNum}")
     @Operation(summary = "카테고리 id별 게시글 보기 페이징")
    public ListResult<WritingItem> getCategoryWritings(@PathVariable long categoryId, @PathVariable  int pageNum){

        return writingService.getCategoryWritings(categoryId,pageNum);
    }
}

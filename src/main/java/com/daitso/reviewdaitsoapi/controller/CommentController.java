package com.daitso.reviewdaitsoapi.controller;

import com.daitso.reviewdaitsoapi.entity.Category;
import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.model.comment.CommentContentChangeRequest;
import com.daitso.reviewdaitsoapi.model.comment.CommentCreateRequest;
import com.daitso.reviewdaitsoapi.model.comment.CommentItem;
import com.daitso.reviewdaitsoapi.model.comment.CommentSearchId;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.service.*;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class CommentController {
    private final CommentService commentService;
    private final WritingService writingService;
    private final MemberService memberService;
    private final CategoryService categoryService;

    @PostMapping("/register/member-id/{memberId}/catergory-id/{categoryId}/writing-id/{writingId}")
    @Operation(summary = "댓글 등록")
    public CommonResult setComment(@RequestBody CommentCreateRequest request,@PathVariable long memberId , @PathVariable long categoryId , @PathVariable long writingId ) {

        Writing writing = writingService.getData(writingId);
        Member member = memberService.getData(memberId);
        Category category = categoryService.getData(categoryId);

        commentService.setComment(request , category, member, writing);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "댓글 리스트 전체보기")
    public ListResult<CommentItem> getComments() {
        return ResponseService.getListResult(commentService.getComments());
    }

    @PutMapping("/content/{id}")
    @Operation(summary = "댓글 내용 수정")
    public CommonResult putCommentContent(@PathVariable long id, @RequestBody CommentContentChangeRequest request) {
        commentService.putCommentContent(id, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/Comment-id/{id}")
    @Operation(summary = "댓글 삭제")
    public CommonResult delComment(long id) {
        commentService.delComment(id);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/search-id/{id}")
    @Operation(summary = "회원id 가 쓴 댓글보기")
    public ListResult<CommentSearchId> searchCommentId(@PathVariable long id) {
        return ResponseService.getListResult(commentService.searchCommentId(id));
    }

    @GetMapping("/latest-order")
    @Operation(summary = "댓글 최신순 보기")
    public ListResult<CommentSearchId> getListOrder(){
        return ResponseService.getListResult(commentService.getListOrder());
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "댓글 1번부터 페이징")
    public ListResult<CommentItem> getCommentsPage(@PathVariable int pageNum){

        return commentService.getCommentsPage(pageNum);
    }

}

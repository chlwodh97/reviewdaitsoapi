package com.daitso.reviewdaitsoapi.controller;


import com.daitso.reviewdaitsoapi.model.member.LoginRequest;
import com.daitso.reviewdaitsoapi.model.member.LoginResponse;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login")
public class LoginController {
    private final LoginService loginService;


    @PostMapping("/member")
    @Operation(summary = "로그인")
    public SingleResult<LoginResponse> doLogin(@RequestBody LoginRequest request){
        return loginService.doLogin(request);
    }
}

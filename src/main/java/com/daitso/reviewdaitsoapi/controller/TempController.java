package com.daitso.reviewdaitsoapi.controller;


import com.daitso.reviewdaitsoapi.enums.MemberLevel;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.service.ResponseService;
import com.daitso.reviewdaitsoapi.service.TempService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;


    @PostMapping("/file-upload")
    @Operation(summary = "파일 대량등록")
    public CommonResult setGoodsByFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
        tempService.setGoodsByFile(csvFile);

        return ResponseService.getSuccessResult();
    }
}

package com.daitso.reviewdaitsoapi.controller;

import com.daitso.reviewdaitsoapi.model.category.CategoryItem;
import com.daitso.reviewdaitsoapi.model.category.CategoryNameChangeRequest;
import com.daitso.reviewdaitsoapi.model.category.CategoryRequest;
import com.daitso.reviewdaitsoapi.model.category.CategoryResponse;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.service.CategoryService;
import com.daitso.reviewdaitsoapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/category")
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/register")
    @Operation(summary = "카테고리 등록")
    public CommonResult setCategory(@RequestBody CategoryRequest request) {
        categoryService.setCategory(request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "카테고리 리스트보기")
    public ListResult<CategoryItem> getCategorys() {
        return ResponseService.getListResult(categoryService.getCategorys());
    }

    @PutMapping("/name/{id}")
    @Operation(summary = "카테고리 이름 수정")
    public CommonResult putCategoryName(@PathVariable long id, @RequestBody CategoryNameChangeRequest request) {
        categoryService.putCategoryName(id, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/category-id/{id}")
    @Operation(summary = "카테고리 id 삭제")
    public CommonResult delCategory(@PathVariable long id) {
        categoryService.delCategory(id);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "카테고리 페이징")
    public ListResult<CategoryItem> getCategorysPage(@PathVariable int pageNum) {

        return categoryService.getCategorysPage(pageNum);
    }

    @GetMapping("/detail/{id}")
    @Operation(summary = "카테고리 상세보기")
    public SingleResult<CategoryResponse> getCategory(@PathVariable long id){
        categoryService.getCategory(id);
        return ResponseService.getSingleResult(categoryService.getCategory(id));
    }
}

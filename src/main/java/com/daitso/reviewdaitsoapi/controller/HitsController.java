package com.daitso.reviewdaitsoapi.controller;


import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.entity.Writing;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.service.HitsService;
import com.daitso.reviewdaitsoapi.service.MemberService;
import com.daitso.reviewdaitsoapi.service.ResponseService;
import com.daitso.reviewdaitsoapi.service.WritingService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/hits")
public class HitsController {
    private final HitsService hitsService;
    private final MemberService memberService;
    private final WritingService writingService;


    @PostMapping("/hitgoods/member-id/{memberId}/writing-id/{writingId}")
    @Operation(summary = "한 번 누르면 추천 또 누르면 추천 삭제")
    public CommonResult setHits(@PathVariable long memberId , @PathVariable long writingId) {

        Member member = memberService.getData(memberId);
        Writing writing = writingService.getData(writingId);

        hitsService.setHits(member , writing);

        return ResponseService.getSuccessResult();


    }
}

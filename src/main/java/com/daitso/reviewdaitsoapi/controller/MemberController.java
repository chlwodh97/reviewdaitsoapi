package com.daitso.reviewdaitsoapi.controller;

import com.daitso.reviewdaitsoapi.entity.Member;
import com.daitso.reviewdaitsoapi.model.member.*;
import com.daitso.reviewdaitsoapi.model.result.CommonResult;
import com.daitso.reviewdaitsoapi.model.result.ListResult;
import com.daitso.reviewdaitsoapi.model.result.SingleResult;
import com.daitso.reviewdaitsoapi.service.MemberService;
import com.daitso.reviewdaitsoapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/register")
    @Operation(summary = "멤버 등록")
    public CommonResult setMember(@RequestBody MemberRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all")
    @Operation(summary = "멤버 리스트 전체보기")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers());
    }

    @GetMapping("/detail/{id}")
    @Operation(summary = "멤버 상세보기")
    public SingleResult<MemberResponse> getMember(@PathVariable long id) {
        memberService.getMember(id);
        return ResponseService.getSingleResult(memberService.getMember(id));
    }

    @PutMapping("/status/{id}")
    @Operation(summary = "멤버 상태 수정")
    public CommonResult putMember(@PathVariable long id, @RequestBody MemberChangeStatusRequest request) {
        memberService.putMemberStatus(id, request);
        return ResponseService.getSuccessResult();
    }

    @PutMapping("/level/{id}")
    @Operation(summary = "멤버 레벨 수정")
    public CommonResult putMember(@PathVariable long id, @RequestBody MemberChangeLevelRequest request) {
        memberService.putMemberLevel(id, request);
        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/delete/member-id/{id}")
    @Operation(summary = "멤버 삭제")
    public CommonResult delMember(@PathVariable long id) {
        memberService.delMember(id);
        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "멤버 10명씩 한 페이지")
    public ListResult<MemberItem> getPageMembers(@PathVariable int pageNum){

        return memberService.getPageMembers(pageNum);
    }
}

package com.daitso.reviewdaitsoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberLevel {
    ROLE_SILVER("실버"),
    ROLE_GOLD("골드"),
    ROLE_PLATINUM("플래티넘"),
    ROLE_DIAMOND("다이아몬드"),
    ROLE_MASTER("마스터");

    // ROLE_ 을 붙이는 이유는 스트링이기 때문에
    // 양식이 없다. -> ROLE_을 붙여 권한인 것을 알 수 있도록
    private final String levelName;
}


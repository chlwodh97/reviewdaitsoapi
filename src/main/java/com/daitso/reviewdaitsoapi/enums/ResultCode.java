package com.daitso.reviewdaitsoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다")
    ,FAILURE(-1, "실패하였습니다");


    private final Integer code;
    private final String msg;
}

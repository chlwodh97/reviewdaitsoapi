package com.daitso.reviewdaitsoapi.enums;


import com.daitso.reviewdaitsoapi.repository.WritingRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Sort;

@Getter
@AllArgsConstructor
public enum SortType {

    NEAR("최근등록순", Sort.Direction.DESC, "id"),
    HIT("추천순", Sort.Direction.DESC, "hitNumber"),
    VIEW("조회순", Sort.Direction.DESC, "viewNumber");

    private final String name;
    private final Sort.Direction direction;
    private final String columnName;


}

package com.daitso.reviewdaitsoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberStatus {
    NORMAL("정상"),
    STOP("정지"),
    OUT("탈퇴");

    private final String statusName;

}



package com.daitso.reviewdaitsoapi;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class ReviewDaitsoApiApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void  testNameLength() {
        String name = "김미나나낭ㅇ";

        int nameLength = name.length();

        System.out.println(nameLength);

        Assert.state(nameLength == 3, "이름은 3글자만 받아용");
    }

}

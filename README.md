# 리뷰 다잇소 프로젝트
### 팀명: daitso (3인)

![첨부 이미지](./images/daitso.png)

### 프로젝트 소개
    다이소를 이용하는 사람들이 조금 더 편하게 다이소를 이용 할 수 있도록 만든 플랫폼 입니다.

    다이소는 매년 매출 및 영업이익이 올라가고 있는 추세입니다.

    매년 이용하는 사람들은 많지만 막상 본인이 찾고있는 물품이 어디에 있는지,

    혹시 비슷한 상품 중에 더 좋은 상품은 없는지,

    이런 점들을 정확히 모르고 이용하는 사람이 많다고 느꼈습니다.

    다이소의 상품만 다뤄 리뷰하는 사이트를 만들자고 생각하였습니다.
### 개발기간 : 2024 - 02.10 ~ 04.08
***
### 백엔드 개발 참여자
* **최재오**
### 사용한 기술
![사용한 기술](./images/Untitled.png)
    Spring boot
    Docker
    PostgreSQL
    Google Cloud Platform(GCP)
    JavaScript
    Vuejs
    Dart
    Flutter

### 프로젝트 아키텍처
![사용한 기술](./images/image.png)

### 프로젝트 주요 기능
* 원하는 상품을 검색 하여 리뷰를 찾아 볼 수 있습니다.
* 궁금한 제품이 어떤 제품인지 리뷰를 통해 알아 볼 수 있습니다.
* ~~상품코드를 통해 원하는 상품을 QR 코드를 통해 찾아볼 수 있습니다~~
* 제품 실제 사용자들과 소통으로 제품의 평가를 알 수 있습니다. (추천기능 등)


### 관련링크
- <a href="https://flowery-clementine-57f.notion.site/Project1-Review-Daitso-4e45e801fde9444fa043d5000f69869f?pvs=4">자세한 설명 링크</a>
- <a href="https://www.youtube.com/watch?v=1CxtEgtYWEc&t=35s">유튜브 영상</a>
